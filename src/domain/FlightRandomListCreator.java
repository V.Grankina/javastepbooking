package domain;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FlightRandomListCreator {
    public static void generate(){
        List <Flight> flightList = new ArrayList<>();
        long now = System.currentTimeMillis();
        Destination[] destination = Destination.values();
        Random random = new Random();
        for (int i = 0; i < 3000; i++) {
            int destinationRandom = random.nextInt(8);
            int durationOfFlight = random.nextInt(45, 240);
            long flightDate = now + 3000000L*i;
            int numberOfSeats = random.nextInt(100);
            Flight flight = new Flight(destination[destinationRandom], durationOfFlight, flightDate, i, numberOfSeats);
            flightList.add(flight);
        }
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("FlightBase"))) {
            objectOutputStream.writeObject(flightList);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
