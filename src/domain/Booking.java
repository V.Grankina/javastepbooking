package domain;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

public class Booking implements Serializable {
    private List<Passenger> passengerList;
    private  int IdOfBooking;
    private Flight flight;

    public Booking(List<Passenger> passengerList, Flight flight, int IdOfBooking) {
        this.passengerList = passengerList;
        this.IdOfBooking = IdOfBooking;
        this.flight = flight;
    }

    public List<Passenger> getPassengerList() {
        return passengerList;
    }

    public void setPassengerList(List<Passenger> passengerList) {
        this.passengerList = passengerList;
    }

    public int getIdOfBooking() {
        return IdOfBooking;
    }

    public void setIdOfBooking(int idOfBooking) {
        IdOfBooking = idOfBooking;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    @Override
    public String toString() {
        return "Booking ID " + IdOfBooking +
                ", flight=" + flight +
                ", passengers: " + passengerList;
    }
}
