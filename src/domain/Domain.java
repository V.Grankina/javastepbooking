package domain;

import DAO.CollectionBookingDAO;
import DAO.ControllerFlightDAO;
import DAO.FlightDAO;
import controllers.BookingController;
import controllers.FlightController;
import exceptions.FlightNotFound;
import services.BookingService;
import services.FlightService;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Domain {
    public void start() {
        ControllerFlightDAO controllerFlightDAO = new ControllerFlightDAO();
        controllerFlightDAO.findAllFlights();
        FlightService flightService = new FlightService(controllerFlightDAO);
        FlightController flightController = new FlightController(flightService);
        List<Booking> bookings = new ArrayList<>();
        CollectionBookingDAO bookingDAO = new CollectionBookingDAO(bookings);
        bookingDAO.findAllBookings();
        BookingService bookingService = new BookingService(bookingDAO);
        BookingController bookingController = new BookingController(bookingService);

        while (true) {
            System.out.println("Введите номер команды:");
            System.out.println("1. Онайн-табло");
            System.out.println("2. Посмотреть информацию о рейсе");
            System.out.println("3. Поиск и бронировка рейса");
            System.out.println("4. Отменить бронирование");
            System.out.println("5. Мои рейсы");
            System.out.println("6. Выход");
            System.out.println("Ваш выбор:");
            int userInt = this.getUserInt(1, 6);
            switch (userInt) {
                case 1:
                    System.out.println("Онайн-табло");
                    flightController.showDepartureTable();
                    this.userContinueConfirmation();
                    break;
                case 2:
                    System.out.println("Введите айди рейса");
                    int flightId = this.getUserInt(0, 9999);
                    Flight flight = flightController.findFlightByID(flightId);
                    System.out.printf("Рейс № %d\nДата и время вылета %s, место назначения %s, длительность полета %d минут,количество свободных мест %d\n",
                            flight.getFligthID(), flight.describeDateAndTime(), flight.getDestination().toString(), flight.getDurationOfFlightInMinutes(),flight.getNumberOfSeats());
                    this.userContinueConfirmation();
                    break;
                case 3:
                    System.out.println("Выберите место назначения из списка");
                    Destination[] destination = Destination.values();
                    for (int i = 0; i < destination.length; i++) {
                        System.out.printf("%d. %s\n", i + 1, destination[i]);
                    }
                    int userDestination = this.getUserInt(1, destination.length);
                    String destinationString = String.valueOf(destination[userDestination - 1]);
                    System.out.println("Введите месяц вылета числом от 1 до 12");
                    int userMonth = this.getUserInt(1, 12);
                    Calendar calendar = new GregorianCalendar();
                    calendar.set(Calendar.YEAR, 2023);
                    calendar.set(Calendar.MONTH, userMonth - 1);
                    System.out.println("Введите день (дату) вылета");
                    int userDay = this.getUserInt(1, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                    calendar.set(Calendar.DAY_OF_MONTH, userDay);
                    System.out.println("Введите количество необходимых билетов");
                    int userNumberOfSeats = this.getUserInt(1, 240);
                    SimpleDateFormat reformat = new SimpleDateFormat("dd.MM.yyyy");
                    String newformat = reformat.format(calendar.getTime());
                    System.out.printf("Ваш запрос: место назначения - %s, дата - %s, количество человек - %d\n", destinationString, newformat, userNumberOfSeats);
                    List<Flight> flightList = flightController.findFlight(destinationString, calendar, userNumberOfSeats);
                    try {
                        if (flightList.size() == 0) {
                            throw new FlightNotFound("Flight Not Found");
                        }
                    } catch (FlightNotFound e) {
                        System.out.println("Ничего не найдено");
                        break;
                    }
                    System.out.println("Вот что мы нашли по Вашему запросу");
                    flightList.forEach(f -> {
                        LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(f.getFligthDate()), ZoneId.systemDefault());
                        String departureTime = date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy 'в' HH:mm"));
                        String destinationOfFlight = f.getDestination().toString();
                        int spaces = 19 - destinationOfFlight.length();
                        System.out.printf("%s  | %s%s| %d \n", departureTime, destinationOfFlight, " ".repeat(Math.max(0, spaces)), f.getFligthID());
                    });
                    int userFligthId;
                    Flight flightToBook = null;
                    do {
                        System.out.println("Введите номер рейса, который хотите забронировать, или нажмите 0 для выхода");
                        userFligthId = this.getUserInt(0, 9999);
                        if (userFligthId == 0) {
                            break;
                        }
                        for (Flight flight1 : flightList) {
                            if (flight1.getFligthID() == userFligthId) {
                                flightToBook = flight1;
                            }
                        }
                        if (flightToBook == null) {
                            System.out.println("Рейс с таким номером не найдено");
                        }
                    }
                    while (flightToBook == null);
                    if (userFligthId == 0) {
                        break;
                    }
                    List<Passenger> passengers = new ArrayList<>();
                    for (int i = 0; i < userNumberOfSeats; i++) {
                        System.out.println("Введите Имя Пассажира №" + (i + 1));
                        String name = this.getUserString("Имя");
                        System.out.println("Введите Фамилию Пассажира №" + (i + 1));
                        String surname = this.getUserString("Фамилию");
                        Passenger passenger = new Passenger(name, surname);
                        passengers.add(passenger);
                    }
                    bookingController.bookAFlight(passengers, flightToBook);
                    flightController.bookAFlight(userFligthId, userNumberOfSeats);
                    System.out.println("Рейс успешно забронировано");
                    this.userContinueConfirmation();
                    break;
                case 4:
                    System.out.println("Введите номер бронирования, который хотите отменить");
                    int idToUnbook = this.getUserInt(0, 10000000);
                    Booking toUnbook = bookingController.findBookingById(idToUnbook);
                    if (toUnbook == null) {
                        System.out.println("Бронирование с таким номером не существует");
                        this.userContinueConfirmation();
                        break;
                    }
                    flightController.unBookAFlight(toUnbook.getFlight().getFligthID(), toUnbook.getPassengerList().size());
                    bookingController.cancelBookingID(idToUnbook);
                    System.out.println("Бронирование номер " + idToUnbook + " успешно отменено!");
                    this.userContinueConfirmation();
                    break;
                case 5:
                    System.out.println("Введите имя пассажира для поиска рейсов");
                    String name = this.getUserString("имя пассажира");
                    System.out.println("Введите фамилию пассажира для поиска рейсов");
                    String surname = this.getUserString("фамилию пассажира");
                    List <Booking> bookings1 = bookingController.showMyFlights(name, surname);
                    if (bookings1.size() == 0){
                        System.out.println("Ничего не найдено");
                    }
                    bookings1.forEach(booking -> {
                        System.out.println("Номер бронирования: " + booking.getIdOfBooking() +
                                " Рейс номер: " + booking.getFlight().getFligthID() +
                                " Время вылета: " + booking.getFlight().describeDateAndTime() +
                                " Направление: " + booking.getFlight().getDestination().name() +
                                " Длительность полета: " + booking.getFlight().getDurationOfFlightInMinutes() + " минут" );
                    });
                    this.userContinueConfirmation();
                    break;
                case 6:
                    System.out.println("До скорых встреч!");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Unknown command");
            }
        }
    }

    int getUserInt(int min, int max){
        Scanner in = new Scanner(System.in);
        String userIntString;
        int userInt = -1;
        do {
            userIntString = in.nextLine();
            if (!userIntString.matches("^[0-9]+[0-9]*$")) {
                System.out.println("Введите целое положительное число");
                continue;
            }
            userInt = Integer.parseInt(userIntString);
            if (userInt < min || userInt > max) {
                System.out.println("Введите целое положительное число не больше " + max);
            }
        } while (!userIntString.matches("^[0-9]+[0-9]*$") || userInt < min || userInt > max);
        return userInt;
    }

    String getUserString(String returnedValue) {
        Scanner in = new Scanner(System.in);
        String userString;
        do {
            userString = in.nextLine();
            String userStringTrimmed = userString.trim();
            userString = userStringTrimmed.substring(0, 1).toUpperCase() + userStringTrimmed.substring(1);
            if (userString.length() < 2) {
                System.out.println("Введите " + returnedValue);
            }
        } while (userString.length() < 2);
        return userString;
    }

    void userContinueConfirmation() {
        System.out.println("Введите 1 чтобы вернутся в главное меню или 0 чтобы закончить работу");
        int userInt = this.getUserInt(0, 1);
        if (userInt == 0) {
            System.exit(0);
        }
    }
}
