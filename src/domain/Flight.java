package domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class Flight implements Serializable {
    private final String departurePoint = "Kiev";
    private Destination destination;

    private int durationOfFlightInMinutes;
    private long fligthDate;
    private int fligthID;
    private int numberOfSeats;

    public Flight(Destination destination, int durationOfFlightInMinutes, long fligthDate, int fligthID, int numberOfSeats) {

        this.destination = destination;
        this.durationOfFlightInMinutes = durationOfFlightInMinutes;
        this.fligthDate = fligthDate;
        this.fligthID = fligthID;
        this.numberOfSeats = numberOfSeats;
    }

    public Flight() {

    }

    public String describeDateAndTime () {
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(this.fligthDate);
        SimpleDateFormat reformat = new SimpleDateFormat("dd.MM.yyyy 'в' HH:mm");
        return reformat.format(calendar.getTime());
    }

    public String getDeparturePoint() {

        return departurePoint;
    }

    public Destination getDestination() {

        return destination;
    }

    public void setDestination(Destination destination) {

        this.destination = destination;
    }

    public int getDurationOfFlightInMinutes() {

        return durationOfFlightInMinutes;
    }

    public void setDurationOfFlightInMinutes(int durationOfFlight) {

        this.durationOfFlightInMinutes = durationOfFlight;
    }

    public long getFligthDate() {
        return fligthDate;
    }

    public void setFligthDate(long fligthDate) {

        this.fligthDate = fligthDate;
    }

    public int getFligthID() {

        return fligthID;
    }

    public void setFligthID(int fligthID) {

        this.fligthID = fligthID;
    }

    public int getNumberOfSeats() {

        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {

        this.numberOfSeats = numberOfSeats;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "departurePoint='" + departurePoint + '\'' +
                ", destination=" + destination +
                ", durationOfFlight=" + durationOfFlightInMinutes +
                ", fligthDate=" + this.describeDateAndTime() +
                ", fligthID=" + fligthID +
                ", numberOfSeats=" + numberOfSeats +
                '}' + "\n";
    }
}
