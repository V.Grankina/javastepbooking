package tests;

import DAO.ControllerFlightDAO;
import domain.Destination;
import domain.Flight;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import services.FlightService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class FlightServiceTest {

    private FlightService module;

    private Flight flight1 = new Flight(Destination.FRANKFURT, 55,
            646274628, 12, 50);
    private Flight flight2 = new Flight(Destination.LONDON, 45,
            64627443, 10, 40);
    private Flight flight3 = new Flight(Destination.FRANKFURT, 225,
            1674624213291L, 2, 42);
    private List<Flight> flightList = new ArrayList<>(List.of(flight1, flight2, flight3));
    private ControllerFlightDAO flightDAO = new ControllerFlightDAO(flightList);

    @BeforeEach
    public void setUp() {

        module = new FlightService(flightDAO);
    }

    @Test
    public void testFindFlightByID() {

        Flight flightTest = module.findFlightByID(12);
        Assertions.assertEquals(flight1.getFligthID(), flightTest.getFligthID());
    }

    @Test
    public void testFindFlightByNonExistentID() {

        Flight flightTest = module.findFlightByID(13);
        Assertions.assertNotEquals(flight1.getFligthID(), flightTest.getFligthID());
    }

    @Test
    public void testFindFlight() {

        List<Flight> flightListTest = new ArrayList<>(List.of(flight3));
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(1674624213291L);
        List<Flight> flightActual = module.findFlight("FRANKFURT", calendar, 5);
        Assertions.assertEquals(flightListTest.get(0).getDestination(), flightActual.get(0).getDestination());
        Assertions.assertEquals(flightListTest.get(0).getFligthDate(), flightActual.get(0).getFligthDate());
        Assertions.assertTrue(flightListTest.get(0).getNumberOfSeats() >= flightActual.get(0).getNumberOfSeats());
    }


}
