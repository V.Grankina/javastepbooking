package tests;

import DAO.CollectionBookingDAO;
import controllers.BookingController;
import domain.Booking;
import domain.Destination;
import domain.Flight;
import domain.Passenger;
import org.junit.jupiter.api.Test;
import services.BookingService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookingControllerTest {
    List<Passenger> passengerList = new ArrayList<>();
    List<Booking> bookingList = new ArrayList<>();
    List<Flight> flightList = new ArrayList<>();
    Passenger passenger = new Passenger("John","Wick");
    int idOfBooking = 1;
    Flight flight = new Flight(Destination.BERLIN,88,7897468,2,60);
    Booking booking = new Booking(passengerList,flight,idOfBooking);
    CollectionBookingDAO collectionBookingDAO = new CollectionBookingDAO(bookingList);
    BookingService bookingService = new BookingService(collectionBookingDAO);
    BookingController bookingController = new BookingController(bookingService);


    @Test
    void bookAFlight() {
        bookingController.bookAFlight(passengerList,flight);
        assertTrue(1 == bookingController.getAllBookings().size());

    }

    @Test
    void getAllBookings() {
        bookingList.add(booking);
        assertTrue(bookingList.equals(bookingController.getAllBookings()));
    }

    @Test
    void cancelBookingID() {
        bookingList.add(booking);
        bookingController.cancelBookingID(1);
        assertEquals(0, bookingController.getAllBookings().size());
    }

//    @Test
//    void showMyFlights() {
//
//    }

    @Test
    void findBookingById() {
        bookingList.add(booking);
        assertTrue(booking.equals(bookingController.findBookingById(1)));
    }
}