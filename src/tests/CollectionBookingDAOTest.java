package tests;

import DAO.CollectionBookingDAO;
import domain.Booking;
import domain.Destination;
import domain.Flight;
import domain.Passenger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CollectionBookingDAOTest {
    Passenger passenger = new Passenger("John","Wick");

    int idOfBooking = 1;

    List<Flight> flightList = new ArrayList<>();
    List<Passenger> passengerList = new ArrayList<>();
    List<Booking> bookingList = new ArrayList<>();
    Flight flight = new Flight(Destination.BERLIN,88,7897468,2,60);
    Booking booking = new Booking(passengerList,flight,idOfBooking);
    CollectionBookingDAO collectionBookingDAO = new CollectionBookingDAO(bookingList);

    @BeforeEach
    void addBooking() {
        collectionBookingDAO.saveBooking(booking);
    }

    @Test
    void saveBooking() {
        List<Booking> bookings = collectionBookingDAO.findAllBookings();
        assertTrue(1 == bookings.size());
    }

    @Test
    void findAllBookings() {
        List<Booking> bookings = new ArrayList<>();
        bookings.add(booking);
        assertTrue(bookings.toString().equals(collectionBookingDAO.findAllBookings().toString()));
    }

    @Test
    void getBookings() {
        List<Booking> bookings = new ArrayList<>();
        bookings.add(booking);
        assertTrue(collectionBookingDAO.getBookings().toString().equals(bookings.toString()));
    }

    @Test
    void deleteBooking() {
        collectionBookingDAO.deleteBooking(1);
        assertTrue(0 == collectionBookingDAO.findAllBookings().size());
    }

    @Test
    void findBookingById() {
        Booking bookingRes = collectionBookingDAO.findBookingById(1);
        assertTrue(booking.getIdOfBooking() == bookingRes.getIdOfBooking());
    }
}