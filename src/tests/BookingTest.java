package tests;

import domain.Booking;
import domain.Destination;
import domain.Flight;
import domain.Passenger;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookingTest {
    Passenger passenger = new Passenger("John", "Wick");
    List<Passenger> passengerList = new ArrayList<>();
    int idOfBooking = 1;
    Flight flight = new Flight(Destination.BERLIN,88,7897468,2,60);
    Booking booking = new Booking(passengerList,flight,idOfBooking);


    @Test
    void testToString() {
        passengerList.add(passenger);
        String actual = booking.toString();
        String expected = "Booking ID " + idOfBooking + ", flight=" + flight + ", passengers: " + passengerList;
        assertEquals(actual,expected);
    }
}