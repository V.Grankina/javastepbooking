package tests;

import DAO.CollectionBookingDAO;
import domain.Booking;
import domain.Destination;
import domain.Flight;
import domain.Passenger;
import org.junit.jupiter.api.Test;
import services.BookingService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookingServiceTest {

    List<Passenger> passengerList = new ArrayList<>();
    List<Booking> bookingList = new ArrayList<>();
    List<Flight> flightList = new ArrayList<>();
    Passenger passenger = new Passenger("John","Wick");
    int idOfBooking = 1;
    Flight flight = new Flight(Destination.BERLIN,88,7897468,2,60);
    Booking booking = new Booking(passengerList,flight,idOfBooking);
    CollectionBookingDAO collectionBookingDAO = new CollectionBookingDAO(bookingList);
    BookingService bookingService = new BookingService(collectionBookingDAO);

    @Test
    void bookAFlight() {
        passengerList.add(passenger);
        flightList.add(flight);
        bookingService.bookAFlight(passengerList,new Flight(Destination.BERLIN,88,7897468,2,60));
        assertTrue(1 == collectionBookingDAO.findAllBookings().size());
    }

    @Test
    void cancelBookingID() {
        bookingList.add(booking);
        bookingService.cancelBookingID(1);
        assertEquals(0,bookingService.getAllBookings().size());
    }

    @Test
    void getAllBookings() {
        collectionBookingDAO.saveBooking(booking);
        List<Booking> bookings = new ArrayList<>();
        bookings.add(booking);
        assertTrue(bookingService.getAllBookings().toString().equals(bookings.toString()));
    }

    @Test
    void showMyFlights() {
        passengerList.add(passenger);
       bookingService.bookAFlight(passengerList, flight);
        assertEquals(0, bookingService.showMyFlights("John", "Doe").size());
       assertTrue(1 == bookingService.showMyFlights("John","Wick").size());
    }

    @Test
    void findBookingById() {
        passengerList.add(passenger);
        flightList.add(flight);
        bookingList.add(booking);
        assertTrue(booking.equals(bookingService.findBookingById(1)));
    }
}