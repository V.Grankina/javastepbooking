package tests;

import domain.Passenger;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PassengerTest {

    Passenger passenger = new Passenger("John","Wick");

    @Test
    void testToString() {
        String expected = "Passenger " + "name: " + passenger.getName() + " surname " + passenger.getSurname();
        String actual = passenger.toString();
        assertEquals(expected,actual);
    }
}