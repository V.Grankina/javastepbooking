package tests;

import domain.Destination;
import domain.Flight;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import services.FlightService;

public class FlightTest {

    private Flight module;

    @BeforeEach
    public void setUp() {

        module = new Flight(Destination.FRANKFURT, 55,
                646274628, 12, 50);
    }

    @Test
    public void testToString (){
        String expected = "Flight{departurePoint='Kiev', destination=FRANKFURT, " +
                "durationOfFlight=55, fligthDate=08.01.1970 в 14:31, fligthID=12, numberOfSeats=50}\n";
        String actual = module.toString();
        Assertions.assertEquals(expected, actual);
    }



}
