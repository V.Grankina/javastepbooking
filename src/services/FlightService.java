package services;

import DAO.ControllerFlightDAO;
import domain.Flight;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class FlightService {

    private ControllerFlightDAO controllerFlightDAO;

    public FlightService(ControllerFlightDAO controllerFlightDAO) {

        this.controllerFlightDAO = controllerFlightDAO;
    }

    public void showDepartureTable() {

        List<Flight> flights = controllerFlightDAO.findAllFlights();
        List<Flight> res = new ArrayList<>();
        long now = System.currentTimeMillis();
        long tomorrow = now + 86400000L;
        flights.forEach(f -> {
            if (f.getFligthDate() <= tomorrow && f.getFligthDate() >= now){
                res.add(f);
            }
        });
        System.out.println("Time                 | Destination        |  Flight");
        res.forEach(f -> {
            String destination = f.getDestination().toString();
            int spaces = 19 - destination.length();
            String spacesString = "";
            for (int i = 0; i < spaces; i++) {
                spacesString += " ";
            }
            System.out.printf("%s  | %s%s| %d \n", f.describeDateAndTime(), destination, spacesString, f.getFligthID());
        });
    }

    public Flight findFlightByID(int id) {

        List<Flight> flights = controllerFlightDAO.findAllFlights();
        List<Flight> res = new ArrayList<>();

        flights.forEach(f -> {
            if (f.getFligthID() == id) {
                res.add(f);
            }
        });
        Flight flight = res.get(0);
        return flight;
    }



    public List<Flight> findFlight(String destination, Calendar userDate, int numberOfSeats) {

        List<Flight> flights = controllerFlightDAO.findAllFlights();
        List<Flight> res = new ArrayList<>();
        flights.forEach(f -> {
            Calendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(f.getFligthDate());
            if (f.getDestination().toString().equalsIgnoreCase(destination) &&
                    calendar.get(Calendar.DAY_OF_MONTH) == userDate.get(Calendar.DAY_OF_MONTH) &&
                    calendar.get(Calendar.MONTH) == userDate.get(Calendar.MONTH)
                    && f.getNumberOfSeats() >= numberOfSeats) {
                res.add(f);
            }
        });
        return res;
    }

    public void bookAFlight(int id, int numberOfSeats) {

        List<Flight> flights = controllerFlightDAO.findAllFlights();
        flights.forEach(f -> {
            if (f.getFligthID() == id) {
                f.setNumberOfSeats(f.getNumberOfSeats() - numberOfSeats);
            }
        });
        controllerFlightDAO.saveFlights();
    }

    public void unBookAFlight(int id, int numberOfSeats) {

        List<Flight> flights = controllerFlightDAO.findAllFlights();
        flights.forEach(f -> {
            if (f.getFligthID() == id) {
                f.setNumberOfSeats(numberOfSeats + f.getNumberOfSeats());
            }
        });
        controllerFlightDAO.saveFlights();
    }


}
