package services;

import DAO.CollectionBookingDAO;
import domain.Booking;
import domain.Flight;
import domain.Passenger;

import java.util.List;


public class BookingService {
    private final CollectionBookingDAO bookingDAO;

    public BookingService(CollectionBookingDAO bookingDAO) {
        this.bookingDAO = bookingDAO;
    }

    public void bookAFlight(List<Passenger> passengerList, Flight flight) {
        List<Booking> bookings = this.bookingDAO.getBookings();
        int length = bookings.size();
        int id = (length == 0) ? 0 : bookings.get(length-1).getIdOfBooking() + 1;
        Booking booking = new Booking(passengerList,flight, id);
        bookingDAO.saveBooking(booking);
    }

    public void cancelBookingID (int bookingID) {
        this.bookingDAO.deleteBooking(bookingID);
    }

    public List<Booking> getAllBookings(){
        return this.bookingDAO.getBookings();
    }

    public List<Booking> showMyFlights (String name, String surname) {
        return bookingDAO.findAllBookings()
                .stream()
                .filter(booking -> {
                    List <Passenger> passengers = booking.getPassengerList();
                    return passengers.stream().anyMatch(passenger -> (passenger.getName().equalsIgnoreCase(name) &&
                            passenger.getSurname().equalsIgnoreCase(surname)));
            }).toList();
    }

    public Booking findBookingById(int id) {
        return bookingDAO.findBookingById(id);
    }
}
