package DAO;

import domain.Flight;

import java.util.List;

public interface FlightDAO {
    public void saveFlights();
    public List<Flight> findAllFlights();

}
