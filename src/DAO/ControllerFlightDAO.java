package DAO;

import domain.Flight;
import domain.FlightRandomListCreator;

import java.io.*;
import java.util.List;

public class ControllerFlightDAO implements FlightDAO, Serializable {

    private List<Flight> flightList;
    private FlightRandomListCreator flightRandomListCreator;


    public ControllerFlightDAO(List<Flight> flightList) {

        this.flightList = flightList;
    }

    public ControllerFlightDAO() {

    }

    @Override
    public void saveFlights() {

        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("FlightBase"))) {
            objectOutputStream.writeObject(flightList);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Flight> findAllFlights() {

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("FlightBase"))) {
            flightList = (List<Flight>) objectInputStream.readObject();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return flightList;
    }


}
