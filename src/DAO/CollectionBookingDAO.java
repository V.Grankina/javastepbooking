package DAO;

import domain.Booking;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionBookingDAO implements BookingDAO, Serializable {
    private List<Booking> bookingList;

    {
        bookingList = new ArrayList<>();
    }

    public CollectionBookingDAO(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }

    @Override
    public void saveBooking(Booking booking) {
        this.bookingList.add(booking);
        this.loadData();
    }

    @Override
    public List<Booking> findAllBookings() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("BookingBase"));
            List <Booking> immutable = (List<Booking>) objectInputStream.readObject();
            bookingList = new ArrayList<>(immutable);
        } catch (IOException e) {
            return bookingList;
        } catch (ClassNotFoundException e) {
            return bookingList;
        }
        return bookingList;
    }

    @Override
    public List<Booking> getBookings() {
        return this.bookingList;
    }

    @Override
    public void deleteBooking(int id) {
        this.bookingList = this.bookingList.stream().filter(b -> b.getIdOfBooking() != id).toList();
        this.loadData();
    }

    @Override
    public Booking findBookingById(int id) {
        Booking booking = null;
        for (Booking b : this.bookingList) {
            if (b.getIdOfBooking() == id) {
                booking = b;
            }
        }
        return booking;
    }

    void loadData() {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("BookingBase"))) {
            objectOutputStream.writeObject(bookingList);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
