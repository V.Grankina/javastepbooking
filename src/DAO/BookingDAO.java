package DAO;

import domain.Booking;

import java.util.List;

public interface BookingDAO {
     void saveBooking(Booking booking);
     List<Booking> findAllBookings();
     List<Booking> getBookings();
     void deleteBooking(int id);

     Booking findBookingById (int id);
}
