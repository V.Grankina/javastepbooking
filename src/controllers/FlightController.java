package controllers;

import domain.Flight;
import services.FlightService;

import java.util.Calendar;
import java.util.List;

public class FlightController {
    private FlightService flightService;

    public FlightController(FlightService flightService) {

        this.flightService = flightService;
    }



    public Flight findFlightByID(int id) {

        return flightService.findFlightByID(id);
    }

    public List<Flight> findFlight(String destination, Calendar userDate, int numberOfSeats) {

        return flightService.findFlight(destination, userDate, numberOfSeats);
    }

    public void bookAFlight(int id, int numberOfSeats) {

        flightService.bookAFlight(id, numberOfSeats);
    }

    public void unBookAFlight(int id, int numberOfSeats) {
        flightService.unBookAFlight(id, numberOfSeats);
    }

    public void showDepartureTable() {
        flightService.showDepartureTable();
    }
}
