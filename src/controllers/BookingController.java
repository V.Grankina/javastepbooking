package controllers;

import domain.Booking;
import domain.Flight;
import domain.Passenger;
import services.BookingService;

import java.util.List;

public class BookingController {
    private BookingService bookingService;

    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    public void bookAFlight(List<Passenger> passengerList, Flight flight) {
        bookingService.bookAFlight(passengerList,flight);
    }

    public List<Booking> getAllBookings() {
        return bookingService.getAllBookings();
    }

    public void cancelBookingID (int bookingID) {
        bookingService.cancelBookingID(bookingID);
    }

    public List<Booking> showMyFlights (String name, String surname) {
        return bookingService.showMyFlights(name, surname);
    }

    public Booking findBookingById(int id) {
        return bookingService.findBookingById(id);
    }
}
